# Hash Visualization

Turn signatures and hashes into interesting phrases. Hash Visualization is an example of Social software, tapping into the user's natural cognitive and social capabilities to create memorable experiences.

## How does it work?

Use a signature and a secret or md5 hash, the program will return a unique and memorable phrase

```bash
>>> python hashviz.py actualdragon#photocopy_of_my_ass
my amphoteric extravert chesapeake bay retriever

>>> python hashviz.py actualdragon#md5hash_of_a_photocopy_of_my_ass
emaciated my ruthful australian mist
```

## Where is this used?

In [xkcd 936 - Password strength](https://www.explainxkcd.com/wiki/index.php/936:_Password_Strength). Hash visualization is used by sites like [gfycat](https://medium.com/@Gfycat/naming-conventions-97960fc40179) and [replit](https://repl.it/site/blog/new_repls) to procedurally generate memorable URL hashes. Bret Victor showed an example of transforming 4Chan's tripcodes into [tripphrases](http://worrydream.com/tripphrase/) which uses a similar method to authenticate an individual into a system like public forums while protecting their anonymity.

## Where can I use it?

Anywhere as long as it makes sense. Just remember to change the data, add more word types, and templates. If you are using this for authentication - don't. As Bret explains it can be brute forced. A place like 4chan or reddit can get away with it.

## License

Hash Visualization is released under MIT license
