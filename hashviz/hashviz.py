#!/usr/bin/env python
import hashlib
import os.path

# Labels should match data file names
WORD_TYPES = ('article', 'adjective', 'animal')

# Mix and match word type labels to create more templates
TEMPLATES = (
    ('article', 'adjective', 'adjective', 'animal'),
    ('adjective', 'article', 'adjective', 'animal')
)

WORDS_BY_TYPE = {}
_base_path = os.path.dirname(__file__)

for wtype in WORD_TYPES:
    with open(os.path.join(_base_path, 'data/%s.txt' % wtype), 'r') as f:
        WORDS_BY_TYPE[wtype] = [w.strip() for w in f.readlines()]


def bytes_to_int(b1, b2, b3, b4):
    return (b1 << 24) | (b2 << 16) | (b3 << 8) | b4

def get_slots(input_str):
    # Converts unicode into a byte string
    if isinstance(input_str, unicode):
        input_str = input_str.encode('UTF-8')

    md5 = hashlib.md5()
    md5.update(input_str)
    digest = [ord(b) for b in md5.digest()]

    return (
        bytes_to_int(*digest[0:4]),
        bytes_to_int(*digest[4:8]),
        bytes_to_int(*digest[8:12]),
        bytes_to_int(*digest[12:16]),
    )

def hashviz(input_str):
    """
    Convert hashes and words into visualizations

    >>> hashviz('actual_dragon#destroyTokyo!')
    'the nonparticulate corruptible toucan'

    >>> hashviz('TheRealDonaldTrump#Make_Walls_Great_Again')
    'doddery our lexical green bee eater'
    """
    # TODO: check if input_str is an md5 hash and skip hash creation
    slots = get_slots(input_str)
    template = TEMPLATES[slots[3] % len(TEMPLATES)]


    words = []
    for idx, wtype in enumerate(template):
        options = WORDS_BY_TYPE[wtype]
        words.append(options[slots[idx] % len(options)])

    return ' '.join(words)

if __name__ == '__main__':
    import sys
    import doctest

    if len(sys.argv) > 1:
        print hashviz(sys.argv[1])
    else:
        doctest.testmod()
    
    sys.exit(0)
