from distutils.core import setup
setup(
	name = 'hashviz',
	packages = ['hashviz'],
	package_dir={'hashviz': 'hashviz'},
	package_data={'hashviz': ['data/*.txt']},
	version = '0.1',
	description = 'Turn signatures and hashes into memorable phrases',
	author = 'Dinesh Raman',
	author_email = 'actualdragon@icloud.com',
	url = 'https://gitlab.com/actualdragon/hashviz',
	download_url = 'https://gitlab.com/actualdragon/hashviz/-/archive/master/hashviz-master.tar.gz',
	keywords = ['hash', 'secret', 'visualize', 'ux', 'url', 'password'],
	classifiers = [],
)